


#backend=package name//databaseConnectFuncs //first class


package backend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import testScripts.driverScript;
import testScripts.generateTestNG;

/* RUN FUNCTIONS IN DIFFERENT DATABASES AS BELOW
System.out.println(dbConnect.RunShortQueryGetData("SELECT * FROM resource_ WHERE ROWNUM <= 10", "ORACLECONSTR", "oracle.jdbc.OracleDriver", "ORCUSERNAME", "ORCPASSWORD"));
System.out.println(dbConnect.RunShortQueryGetData("SELECT top 10 * FROM gbprice", "SQLSERVERCONSTR", "com.microsoft.sqlserver.jdbc.SQLServerDriver", "USERNAME", "PASSWORD"));
*/

public class databaseConnectFuncs {

	public String QueryDbPutDataTempFile(String query,
			String connectionStringProp, String str_forName, String requestPath, String fileName, String str_userName, String str_pwd)
			throws IOException, SQLException {

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		Properties prop = new Properties();
		FileInputStream readPropFile = null;
		readPropFile = new FileInputStream(generateTestNG.str_folderPath+"\\const.properties");
		prop.load(readPropFile);
		List<List<String>> datalist = new LinkedList<List<String>>();

		List<String> rowList = new ArrayList<String>();

		// System.out.println(query);

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		try {
			dbConnection = getDBConnection(connectionStringProp, str_forName, str_userName, str_pwd);

			preparedStatement = dbConnection.prepareStatement(query,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			// execute select SQL statement
			ResultSet rs = preparedStatement.executeQuery();

			rs.last();

			rs.beforeFirst();

			PrintWriter csvWriter = new PrintWriter(new File(requestPath
					+ fileName));
			ResultSetMetaData meta = rs.getMetaData();
			int numberOfColumns = meta.getColumnCount();
			String dataHeaders = "\"" + meta.getColumnName(1) + "\"";
			for (int i = 2; i < numberOfColumns + 1; i++) {
				dataHeaders += ",\"" + meta.getColumnName(i) + "\"";
			}
			csvWriter.println(dataHeaders);

			while (rs.next()) {

				String row = "\"" + rs.getString(1) + "\"";
				for (int i = 2; i < numberOfColumns + 1; i++) {
					row += ",\"" + rs.getString(i) + "\"";
				}
				csvWriter.println(row);

			}

			csvWriter.close();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		return fileName;

	}

	public String QuerySQLFilePutDataTempFile(String sqlFileName,
			String connectionStringProp, String str_forName, String requestPath, String fileName, String str_userName, String str_pwd)
			throws IOException, SQLException {

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		String query = "";
		Properties prop = new Properties();
		FileInputStream readPropFile = null;
		readPropFile = new FileInputStream("paths.properties");
		prop.load(readPropFile);
		List<List<String>> datalist = new LinkedList<List<String>>();

		List<String> rowList = new ArrayList<String>();

		try {

			BufferedReader bf = new BufferedReader(new FileReader(
					prop.getProperty("DIRPATHCONFIGSQL") + sqlFileName));

			StringBuilder sb = new StringBuilder();

			String line = "";

			while ((line = bf.readLine()) != null) {

				sb.append(line);

				sb.append("\n ");

			}
			query = sb.toString();
						
			//query=	sb.toString().replace("(select max(date) from DateManager where status=1)", "'"+param+"'");
			
/*			if(query.contains("(select max(date) from DateManager where status=1)")){
				query=	sb.toString().replace("(select max(date) from DateManager where status=1)", "'"+param+"'");
						
				
			}else{
				query = sb.toString();
						
			}
*/
			if(bf!=null){
				bf.close();
			}
		}catch (FileNotFoundException e) {

			e.printStackTrace();

		}

		// System.out.println(query);

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		try {
			dbConnection = getDBConnection(connectionStringProp, str_forName, str_userName, str_pwd);

			preparedStatement = dbConnection.prepareStatement(query,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			// execute select SQL statement
			ResultSet rs = preparedStatement.executeQuery();

			rs.last();

			rs.beforeFirst();

			PrintWriter csvWriter = new PrintWriter(new File(requestPath
					+ fileName));
			ResultSetMetaData meta = rs.getMetaData();
			int numberOfColumns = meta.getColumnCount();
			String dataHeaders = "\"" + meta.getColumnName(1) + "\"";
			for (int i = 2; i < numberOfColumns + 1; i++) {
				dataHeaders += ",\"" + meta.getColumnName(i) + "\"";
			}
			csvWriter.println(dataHeaders);

			while (rs.next()) {

				String row = "\"" + rs.getString(1) + "\"";
				for (int i = 2; i < numberOfColumns + 1; i++) {
					row += ",\"" + rs.getString(i) + "\"";
				}
				csvWriter.println(row);

			}

			csvWriter.close();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		return fileName;

	}

	public List<List<String>> QuerySQLFileReturnData(String sqlFileName
			, String connectionStringProp, String str_forName, String str_userName, String str_pwd) throws IOException, SQLException {

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		String query = "";
		Properties prop = new Properties();
		FileInputStream readPropFile = null;
		readPropFile = new FileInputStream("paths.properties");
		prop.load(readPropFile);
		List<List<String>> datalist = new LinkedList<List<String>>();

		List<String> rowList = new ArrayList<String>();

		try {

			BufferedReader bf = new BufferedReader(new FileReader(
					prop.getProperty("DIRPATHCONFIGSQL") + sqlFileName));

			StringBuilder sb = new StringBuilder();

			String line = "";

			while ((line = bf.readLine()) != null) {

				sb.append(line);

				sb.append("\n ");

			}
//			query=	sb.toString().replace("(select max(date) from DateManager where status=1)", "'"+param+"'");
			
//			if(query.contains("(select max(date) from DateManager where status=1)")){
//				query=	sb.toString().replace("(select max(date) from DateManager where status=1)", "'"+param+"'");
//						
//				
//			}else{
				query = sb.toString();
//						
//			}

		}

		catch (FileNotFoundException e) {

			e.printStackTrace();

		}

		// System.out.println(query);

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		try {
			dbConnection = getDBConnection(connectionStringProp, str_forName, str_userName, str_pwd);

			preparedStatement = dbConnection.prepareStatement(query,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			// execute select SQL statement
			ResultSet rs = preparedStatement.executeQuery();

			int rows = 0;
			int columns = rs.getMetaData().getColumnCount();

			rs.last();
			rows = rs.getRow();

			rs.beforeFirst();

			while (rs.next()) {

				for (int i = 1; i <= columns; i++) {

					rowList.add(rs.getString(i));

				}

			}

			int partitionSize = columns;

			for (int i = 0; i < rowList.size(); i += partitionSize) {
				datalist.add(rowList.subList(i,
						Math.min(i + partitionSize, rowList.size())));
			}

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		return datalist;

	}
	
	public String RunShortQueryPutDataTempFile(String query,
			String connectionStringProp, String str_forName, String requestPath, String fileName, String str_userName, String str_pwd)
			throws IOException, SQLException {

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		Properties prop = new Properties();
		FileInputStream readPropFile = null;
		readPropFile = new FileInputStream("paths.properties");
		prop.load(readPropFile);
		List<List<String>> datalist = new LinkedList<List<String>>();

		List<String> rowList = new ArrayList<String>();



		// System.out.println(query);

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		try {
			dbConnection = getDBConnection(connectionStringProp, str_forName, str_userName, str_pwd);
			
			preparedStatement = dbConnection.prepareStatement(query,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			// execute select SQL statement
			ResultSet rs = preparedStatement.executeQuery();

			rs.last();

			rs.beforeFirst();

			PrintWriter csvWriter = new PrintWriter(new File(requestPath
					+ fileName));
			ResultSetMetaData meta = rs.getMetaData();
			int numberOfColumns = meta.getColumnCount();
			String dataHeaders = "\"" + meta.getColumnName(1) + "\"";
			for (int i = 2; i < numberOfColumns + 1; i++) {
				dataHeaders += ",\"" + meta.getColumnName(i) + "\"";
			}
			csvWriter.println(dataHeaders);

			while (rs.next()) {

				String row = "\"" + rs.getString(1) + "\"";
				for (int i = 2; i < numberOfColumns + 1; i++) {
					row += ",\"" + rs.getString(i) + "\"";
				}
				csvWriter.println(row);

			}

			csvWriter.close();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		return fileName;

	}
	
	public void RunShortQueryGetDataList(String query,
			String connectionStringProp, String str_forName, String str_userName, String str_pwd) throws IOException, SQLException {

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		//List<String> rowList = new ArrayList<String>();

		// System.out.println(query);

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		try {
			dbConnection = getDBConnection(connectionStringProp, str_forName, str_userName, str_pwd);

			preparedStatement = dbConnection.prepareStatement(query,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			// execute select SQL statement
			ResultSet rs = preparedStatement.executeQuery();

			int rows = 0;
			int columns = rs.getMetaData().getColumnCount();

			rs.last();
			rows = rs.getRow();

			rs.beforeFirst();

			while (rs.next()) {

				for (int i = 1; i <= columns; i++) {

					//rowList.add(rs.getString(i));

				}

			}

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		//return rowList;

	}

	public List<List<String>> RunShortQueryGetData(String query,
			String connectionStringProp, String str_forName, String str_userName, String str_pwd) throws IOException, SQLException {

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		List<List<String>> datalist = new LinkedList<List<String>>();

		List<String> rowList = new ArrayList<String>();

		// System.out.println(query);

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		try {
			dbConnection = getDBConnection(connectionStringProp, str_forName, str_userName, str_pwd);

			preparedStatement = dbConnection.prepareStatement(query,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			// execute select SQL statement
			ResultSet rs = preparedStatement.executeQuery();

			int rows = 0;
			int columns = rs.getMetaData().getColumnCount();

			rs.last();
			rows = rs.getRow();

			rs.beforeFirst();

			while (rs.next()) {

				for (int i = 1; i <= columns; i++) {

					rowList.add(rs.getString(i));

				}

			}

			int partitionSize = columns;

			for (int i = 0; i < rowList.size(); i += partitionSize) {
				datalist.add(rowList.subList(i,
						Math.min(i + partitionSize, rowList.size())));
			}

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		return datalist;

	}

	public void QuerySQLFileNoDataReturn(String sqlFileName,
			String connectionStringProp, String str_forName, String str_userName, String str_pwd) throws IOException, SQLException {

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

		String query = "";
		Properties prop = new Properties();
		FileInputStream readPropFile = null;
		readPropFile = new FileInputStream("paths.properties");
		prop.load(readPropFile);


		try {

			BufferedReader bf = new BufferedReader(new FileReader(
					prop.getProperty("DIRPATHCONFIGSQL") + sqlFileName));

			StringBuilder sb = new StringBuilder();

			String line = "";

			while ((line = bf.readLine()) != null) {

				sb.append(line);

				sb.append("\n ");

			}
			
			query = sb.toString();
			
			//query=	sb.toString().replace("(select max(date) from DateManager where status=1)", "'"+param+"'");
						
/*			if(query.contains("(select max(date) from DateManager where status=1)")){
				query=	sb.toString().replace("(select max(date) from DateManager where status=1)", "'"+param+"'");
						
				
			}else{
				
						
			}
*/
		}

		catch (FileNotFoundException e) {

			e.printStackTrace();

		}

		System.out.println(query);

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		try {
			dbConnection = getDBConnection(connectionStringProp, str_forName, str_userName, str_pwd);

			preparedStatement = dbConnection.prepareStatement(query,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = preparedStatement.executeQuery();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}
		
		

		System.out
				.println("Now quering the database and getting data in a 2D arrayList");

	}

	protected static Connection getDBConnection(String connectionStringProp, String str_forName, String str_userName, String str_pwd)
			throws IOException {

		System.out.println("Now establishing a DB Connection.");

		Properties prop = new Properties();
		FileInputStream readPropFile = null;
		readPropFile = new FileInputStream("const.properties");
		prop.load(readPropFile);
		String st = prop.getProperty(connectionStringProp);
		String UserName = prop.getProperty(str_userName);
		String Password = prop.getProperty(str_pwd);

		Connection dbConnection = null;

		try {
			//Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			//Class.forName("oracle.jdbc.OracleDriver");
			Class.forName(str_forName);

		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}

		try {

			if(str_forName.contains("oracle")){
				dbConnection = DriverManager.getConnection(st, UserName, Password);
				return dbConnection;
			}else if(str_forName.contains("SQLServer")){
				dbConnection = DriverManager.getConnection(st);
				return dbConnection;
			}

		} catch (SQLException e) {
			
			System.out.println(e.getMessage());

		}

		System.out
				.println("Done establishing the connection returing the object.");

		return dbConnection;

	}

}





package backend;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.Logs;

public class ReadConsoleLogsFuncs {

	public String getLog(WebDriver driver){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		try {
			LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
			if (logEntries != null) {
				StringBuilder logBuilder = new StringBuilder();
				for (LogEntry logEntry : logEntries)
					logBuilder
							.append(dateFormat.format(new Date(logEntry
									.getTimestamp()))).append(": ")
							.append(logEntry.getMessage()).append("\r\n");

				logBuilder.toString();
				System.out.println(logBuilder);
			}
			return null;
		} catch (Exception e) {
			// log.info( "Could not generate device logs" );
			return null;
		}
	}
	
	//Done
	public void dumpLog(WebDriver driver) throws Exception {
		Logs log = driver.manage().logs();
		LogEntries entries = log.get(LogType.BROWSER);
		// System.out.println(entries);
		List<LogEntry> list = entries.getAll();
		try{
			for (int i = 0; i < list.size(); i++) {
				LogEntry e = list.get(i);
				//System.out.println(e);
				if (e.getLevel().getName().equals("SEVERE") 
						&& e.getMessage().indexOf("Uncaught ") != -1
						&& e.getMessage().indexOf(" Error:") != -1) {
					System.out.println("*** Uncaught Error ***");
				}		    			
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public String getLastConsoleAlertMessage(WebDriver driver, String ALERT_KEY) {
	    String msg;
	    List<LogEntry> l = driver.manage().logs().get(LogType.BROWSER).getAll();
	    for (int i = l.size() - 1; i >= 0; i--) {
	        if (l.get(i).getMessage().contains(ALERT_KEY)) {
	            msg = l.get(i).getMessage();
	            return msg.substring(msg.indexOf('"') + 1, msg.length() - 1).replace(ALERT_KEY, "").replace(" (:", "");
	        }
	    }
	    return null;
	}
	
	public void getAvailableLogTypes(WebDriver driver) {

	    Options manage = driver.manage();
	    Logs logs = manage.logs();
	    Set<String> availableLogTypes = logs.getAvailableLogTypes();
	    System.out.println(availableLogTypes);
	    
		for (String logType : availableLogTypes) {
			System.out.println("logType=" + logType);
			LogEntries logEntries = logs.get(logType);
			List<LogEntry> all = logEntries.getAll();
			for (LogEntry entry : all) {

				System.out.println(entry.getLevel());
				System.out.println(entry.getMessage());
				System.out.println(entry.getTimestamp());
				System.out.println(entry.toMap());
				System.out.println(entry);
			}
			List<LogEntry> filter = logEntries.filter(Level.SEVERE);
			System.out.println("After filtering");
			for(LogEntry x : filter){
				System.out.println(x);				
			}
			System.out.println("Done");
		}
	}
	 
		 
}



//java code packageanme
//class name=CommonFunctions


package javaCode;
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.binary.Base64;

// Run TrippleDes.java file and give the userid or password to get the encrypt format.


public class Decrypt{

    private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private KeySpec ks;
    private SecretKeyFactory skf;
    private Cipher cipher;
    byte[] arrayBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    SecretKey key;

    public Decrypt() throws Exception {
        myEncryptionKey = "ThisIsSpartaThisIsSparta";
        myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
        arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
        ks = new DESedeKeySpec(arrayBytes);
        skf = SecretKeyFactory.getInstance(myEncryptionScheme);
        cipher = Cipher.getInstance(myEncryptionScheme);
        key = skf.generateSecret(ks);
    }


    public String encrypt(String unencryptedString) {
        String encryptedString = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] encryptedText = cipher.doFinal(plainText);
            encryptedString = new String(Base64.encodeBase64(encryptedText));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return encryptedString;
    }


    public String decrypt(String encryptedString) {
        String decryptedText=null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedText = Base64.decodeBase64(encryptedString);
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText= new String(plainText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedText;
    }


    public static String DecryptData(String encryptedData) throws Exception
    {
        Decrypt td= new Decrypt();
        String decrypted=td.decrypt(encryptedData);
//        System.out.println("Decrypted String:" + decrypted);
        return decrypted;

    }

}


//3rd class







package javaCode;

public class GenericPair<K, V> {

	    private K key;

	    private V value;

	    public GenericPair(K key, V value) {
	        this.key = key;
	        this.value = value;
	    }

	    public K getKey() {
	        return key;
	    }

	    public void setKey(K key) {
	        this.key = key;
	    }

	    public V getValue() {
	        return value;
	    }

	    public void setValue(V value) {
	        this.value = value;
	    }

	   /* How to use:
	    * public static void main(String[] args) {
	    	WebElement x = null;
	        GenericPair<WebElement, String> pair = new GenericPair<WebElement, String>(x, "value");
	        System.out.println(pair.getKey());
	        System.out.println(pair.getValue());
	    }*/

}


//another class

package javaCode;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
    
public class HighlightElement{
	
	 public static void highlightMe(WebDriver driver,WebElement element) throws InterruptedException{
		  //Creating JavaScriptExecuter Interface
		   JavascriptExecutor js = (JavascriptExecutor)driver;
		   for (int iCnt = 0; iCnt < 5; iCnt++) {
		      //Execute javascript
		         js.executeScript("arguments[0].style.border='4px groove red'", element);
		         Thread.sleep(400);
		         js.executeScript("arguments[0].style.border=''", element);
		   }
		 }	

}



//another class


package javaCode;

import java.io.IOException;

import objectRepository.Objects;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;

import reporting.Report;
import testScripts.driverScript;

public class LinksDeepLinking {

	
    private static void fail(WebDriver driver, String str_UserData, String fileName,
            String str_TestNavigation, String str_TestTitle,
            String str_Expected, String str_Actual)
            throws InvalidFormatException, IOException {
     String str_fileName = fileName;
     String str_screenShotFileName = CommonFunctions.takeScreenShotMethod(driver, str_fileName);
     Report.failDetailedReporting(driver, str_screenShotFileName, str_TestNavigation, str_TestTitle, "Fail", str_Expected, str_Actual);
     Report.Log_ReportNG("FAIL", str_screenShotFileName, str_TestNavigation, str_TestTitle, str_Expected, str_Actual);
     System.err.println(str_TestNavigation+ " : " +str_TestTitle+ " : " +"Fail"+ " : " +str_Expected+ " : " +str_Actual);
     int count = 0;
     
     if(driverScript.failedStepsMap.containsKey(str_UserData)){
            count = driverScript.failedStepsMap.get(str_UserData);
            driverScript.failedStepsMap.remove(str_UserData);
            driverScript.failedStepsMap.put(str_UserData, count + 1);
     }

}

private static void pass(WebDriver driver, String str_UserData,  String str_TestNavigation,
            String str_TestTitle) throws InvalidFormatException, IOException {
     Report.Log_ReportNG("PASS", "", str_TestNavigation, str_TestTitle, "Pass");
     Report.passDetailedReporting(str_TestNavigation, str_TestTitle, "Pass");
     System.out.println(str_TestNavigation+" : "+str_TestTitle+" : Pass");
     int count = 0;
     
     if(driverScript.passedStepsMap.containsKey(str_UserData)){
            count = driverScript.passedStepsMap.get(str_UserData);
            driverScript.passedStepsMap.remove(str_UserData);
            driverScript.passedStepsMap.put(str_UserData, count + 1);
     }

}

	public static void takePageScreenShot(WebDriver driver, String str_UserData, String str_URL, String str_pageLink, String str_pageTitle){

		try{
			CommonFunctions.NavigateToPage_Direct(driver, str_URL+str_pageLink, 10000);
			fail(driver, str_UserData, "test_fileName","Test Navigation", "Test Title",
		            "Expected", "Actual");
			pass(driver, str_UserData,"Test Navigation", "Test Title");
		}catch(Exception e){

			
		}
	}
	
}



//another class



package javaCode;

import java.util.List;

public class SortingAndSearching {

	private List<String> lst_str;

	public SortingAndSearching(){
	}

	public SortingAndSearching(List<String> list){
		lst_str = list;
	}
	
	private boolean asc(String value1, String value2){
		return value1.compareTo(value2)>0;
	}

	private boolean desc(String value1, String value2){
		return value1.compareTo(value2)<0;
	}
	
	//Searching through an list
	public int BinarySearch(String value){
		int size = lst_str.size();
		int low = 0;
		int high = size - 1;
		int mid;
		while(low<=high){
			
			mid = low + (high-low)/2;
			
			if(lst_str.get(mid).equals(value))
				return mid;
			else if(lst_str.get(mid).compareTo(value)<0)
				low = mid + 1;
			else
				high = mid - 1;
		}
		
		return -1;
	}

	//Selecting kth max element from an list
	public int QuickSelect(int arr[], int l, int r, int k){

	    // If k is smaller than number of 
	    // elements in array
	    if (k > 0 && k <= r - l + 1) {
	    	int swap;
	        // Partition the array around last 
	        // element and get position of pivot 
	        // element in sorted array
	    	int x = arr[r], i = l;
		    for (int j = l; j <= r - 1; j++) {
		        if (arr[j] <= x) {
		            arr[i] = arr[j];
		            i++;
		        }
		    }
		    arr[i] = arr[r];
	        int index = i;
	 
	        // If position is same as k
	        if (index - l == k - 1)
	            return arr[index];
	 
	        // If position is more, recur 
	        // for left subarray
	        if (index - l > k - 1) 
	            return QuickSelect(arr, l, index - 1, k);
	 
	        // Else recur for right subarray
	        return QuickSelect(arr, index + 1, r, k - index + l - 1);
	    }else{
		    // If k is more than number of 
		    // elements in array
		    return -1;	    
	    	
	    }
	}
	
	//Sort type 1: O(n2)
	public void BubbleSort(boolean isAscending) {

		int size = lst_str.size();
		int i, j, swapped = 1;
		String temp;

		for (i = 0; i < (size - 1) && swapped == 1; i++) {
			swapped = 0;
			for (j = 0; j < (size - i - 1); j++) {
				if (isAscending) {
					if (asc(lst_str.get(j), lst_str.get(j + 1))) {
						temp = lst_str.get(j);
						lst_str.set(j, lst_str.get(j + 1));
						lst_str.set(j + 1, temp);
						swapped = 1;
						//System.out.println(lst_str);
					}
				} else {
					if (desc(lst_str.get(j), lst_str.get(j + 1))) {
						temp = lst_str.get(j);
						lst_str.set(j, lst_str.get(j + 1));
						lst_str.set(j + 1, temp);
						swapped = 1;
						//System.out.println(lst_str);
					}
				}
			}
		}
	}

	//Sort type 2: O(n2)
	public void InsertionSort(boolean isAscending){

		int size = lst_str.size();
		
		int j;
		String temp;
		
		for(int i=1;i<size;i++){
			temp = lst_str.get(i);
			if(isAscending){
				for(j=i;j>0 && asc(lst_str.get(j-1),temp);j--){
					lst_str.set(j,lst_str.get(j-1) );
				}
			}else{
				for(j=i;j>0 && desc(lst_str.get(j-1),temp);j--){
					lst_str.set(j,lst_str.get(j-1) );
				}
			}
			lst_str.set(j,temp);
			//System.out.println(lst_str);
		}
	}

	//Sort type 3: O(n2)
	public void SelectionSort(boolean isAscending) {
		
		for (int i = 0; i < lst_str.size()-1; i++) {
			int min_idx = i;
			for (int j = i+1; j < lst_str.size(); j++) {
				if(isAscending){
					if (lst_str.get(j).compareTo(lst_str.get(min_idx)) < 0){
						min_idx = j;
					}
				}else{
					if (lst_str.get(j).compareTo(lst_str.get(min_idx)) > 0){
						min_idx = j;
					}
				}
			}
			String temp = lst_str.get(min_idx);
			lst_str.set(min_idx, lst_str.get(i));
			lst_str.set(i, temp);
			//System.out.println(lst_str);
		}
	}
	
	//Sort type 4: O(n*log(n))
	public void MergeSort(){
		
	}
	
	//Sort type 5: O(n*log(n))
	public void HeapSort(){
		
	}

	//Sort type 6: O(n2)
	public void QuickSort(){
		
	}

	//Sort type 7: O(n+k)
	public void BucketSort(){
		
	}

	//Sort type 8: O(n2) <- Worst case; O(n*log(n)) <- Average case
	public void TreeSort(){
		
	}
	
}




//another class

package objectRepository;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Objects {

	public boolean isPageEntitled(WebDriver driver, String str_pageLink){
		List<WebElement> ele_potletContentList = driver.findElements(By.className("portlet-content"));
		for(int i=0;i<ele_potletContentList.size();i++){
			if(ele_potletContentList.get(i).getText().contains("not entitled") && ele_potletContentList.get(i).getText().contains(str_pageLink)){
				return false;
			}
		}
		return true;
	}
	
	public boolean isLoadingData(WebDriver driver) throws InterruptedException{
		List<WebElement> ele_divList = driver.findElements(By.tagName("div"));
		for(int i=0;i<ele_divList.size();i++){
			if(ele_divList.get(i).getText().contains("Loading...")){
				return true;
			}
		}
		return false;
	}
	
}



//package reporting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javaCode.CommonFunctions;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.xml.XmlSuite;

import testScripts.driverScript;
import testScripts.generateTestNG;

public class CustomTestNGReporter implements IReporter {
	
	//This is the customize emailabel report template file path.
	private static final String emailableReportTemplateFile = generateTestNG.str_folderPath+"src/test/java/reporting/customize-emailable-report-template.html";
	
	private static int count = 1;
	
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		
		try
		{
			// Get content data in TestNG report template file.
			String customReportTemplateStr = this.readEmailabelReportTemplate();
			
			// Create custom report title.
			//String customReportTitle = this.getCustomReportTitle("Custom TestNG Report");
			
			// Create test suite summary data.
			String customSuiteSummary = this.getTestSuiteSummary(suites);
			
			// Create test methods summary data.
			String customTestMethodSummary = this.getTestMehodSummary(suites);
			
			// Replace report title place holder with custom title.
			//customReportTemplateStr = customReportTemplateStr.replaceAll("\\$TestNG_Custom_Report_Title\\$", customReportTitle);
			
			// Replace test suite place holder with custom test suite summary.
			customReportTemplateStr = customReportTemplateStr.replaceAll("\\$Test_Case_Summary\\$", customSuiteSummary);
			
			// Replace test methods place holder with custom test method summary.
			customReportTemplateStr = customReportTemplateStr.replaceAll("\\$Test_Case_Detail\\$", customTestMethodSummary);
			
			// Write replaced test report content to custom-emailable-report.html.
			File targetFile = new File(outputDirectory + "/custom-emailable-report.html");
			FileWriter fw = new FileWriter(targetFile);
			fw.write(customReportTemplateStr);
			fw.flush();
			fw.close();
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	/* Read template content. */
	private String readEmailabelReportTemplate()
	{
		StringBuffer retBuf = new StringBuffer();
		
		try {
		
			File file = new File(this.emailableReportTemplateFile);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			String line = br.readLine();
			while(line!=null)
			{
				retBuf.append(line);
				line = br.readLine();
			}
			
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}finally
		{
			return retBuf.toString();
		}
	}
	
	/* Build custom report title. */
	private String getCustomReportTitle(String title)
	{
		StringBuffer retBuf = new StringBuffer();
		retBuf.append(title + " " + this.getDateInStringFormat(new Date()));
		return retBuf.toString();
	}
	
	/* Build test suite summary data. */
	private String getTestSuiteSummary(List<ISuite> suites) throws IOException
	{
		
		 String str_TestDataFileName = driverScript.fileName;
		String str_TestDataSheetName =  "TestNameMapping";
		
		// Read data from TestSet and Pass -->  Test Name if Execute is Y; Env; Browser
		
		String testData_filePath = generateTestNG.dataFilePath;
		String fileName = str_TestDataFileName; //"HybridFrameWork_TestData.xlsx";
		String sheetName_TS = str_TestDataSheetName; //"TestSet";
		
		//Create an object of File class to open xlsx file
		File file_TS =    new File(testData_filePath+fileName);
		
		//Create an object of FileInputStream class to read excel file
		FileInputStream inputStream = new FileInputStream(file_TS);
		Workbook myWorkBook = null;
		
		//Find the file extension by splitting file name in substring and getting only extension name
		String fileExtensionName = fileName.substring(fileName.indexOf("."));

		//Check condition if the file is xlsx file
		if(fileExtensionName.equals(".xlsx")){
		    //If it is xlsx file then create object of XSSFWorkbook class
			myWorkBook = new XSSFWorkbook(inputStream);
		}
		//Check condition if the file is xls file
		else if(fileExtensionName.equals(".xls")){
		    //If it is xls file then create object of XSSFWorkbook class
			myWorkBook = new HSSFWorkbook(inputStream);
		}				
		Sheet mySheet_TS = myWorkBook.getSheet(sheetName_TS);

		int rowCount_TS = mySheet_TS.getLastRowNum()- mySheet_TS.getFirstRowNum();

		int int_CellIndex_AutString = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "AutomationString");				
		int int_CellIndex_ReportingTestName = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "ReportingTestName");				
		
		
		StringBuffer retBuf = new StringBuffer();
		
		try
		{
			int totalTestCount = 0;
			int totalTestPassed = 0;
			int totalTestFailed = 0;
			int totalTestSkipped = 0;
			
			for(ISuite tempSuite: suites)
			{
				//retBuf.append("<tr><td colspan=11><center><b>" + tempSuite.getName() + "</b></center></td></tr>");
				
				Map<String, ISuiteResult> testResults = tempSuite.getResults();

				for (ISuiteResult result : testResults.values()) {
					retBuf.append("<tr>");
					String str_reportingTestName = "";
					ITestContext testObj = result.getTestContext();
					
					for (int iRow_TS=1;iRow_TS<=rowCount_TS;iRow_TS++)
					{
						String str_CellIndex_AutString = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_AutString).toString();

						if(str_CellIndex_AutString.equals(testObj.getName())){
							str_reportingTestName = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_ReportingTestName).toString();
						}
						
					}
					
					totalTestPassed = testObj.getPassedTests().getAllMethods().size();
					totalTestSkipped = testObj.getSkippedTests().getAllMethods().size();
					totalTestFailed = testObj.getFailedTests().getAllMethods().size();
					
					totalTestCount = totalTestPassed + totalTestSkipped + totalTestFailed;
					
					/* Test name. */
					retBuf.append("<td>");
					retBuf.append(str_reportingTestName);
					retBuf.append("</td>");

					System.err.println("__________*********************__________");
					System.err.println(testObj.getName());
					System.err.println(driverScript.passedStepsMap);
					System.err.println(driverScript.passedStepsMap.get(testObj.getName()));
					System.err.println(testObj.getName());
					System.err.println("__________*********************__________");

					/* Include groups. */
					retBuf.append("<td>");
					if(driverScript.failedStepsMap.containsKey(testObj.getName())){
						retBuf.append(driverScript.passedStepsMap.get(testObj.getName()));
					}
					retBuf.append("</td>");
					
					/* Exclude groups. */
					retBuf.append("<td><a href=\"http://cft-devops.apps.ml.com/mercury/dev/job/QA/job/glis-test-automation/ws/target/test-output/html/suite1_test"+(count++)+"_results.html\">");
					if(driverScript.failedStepsMap.containsKey(testObj.getName())){
						retBuf.append(driverScript.failedStepsMap.get(testObj.getName()));
					}
					retBuf.append("</a></td>");
					
/*					 Total method count. 
					retBuf.append("<td>");
					retBuf.append(totalTestCount);
					retBuf.append("</td>");
*/					
					/* Passed method count. */
					/*retBuf.append("<td bgcolor=green>");
					retBuf.append(totalTestPassed);
					retBuf.append("</td>");
					*/
/*					 Skipped method count. 
					retBuf.append("<td bgcolor=yellow>");
					retBuf.append(totalTestSkipped);
					retBuf.append("</td>");
*/					
					
					/* Failed method count. 
					retBuf.append("<td bgcolor=red>");
					retBuf.append(totalTestFailed);
					retBuf.append("</td>");
					*/
					/* Get browser type. */
					String browserType = tempSuite.getParameter("browserType");
					if(browserType==null || browserType.trim().length()==0)
					{
						try{
							browserType = testObj.getName().substring(0,testObj.getName().indexOf("#"));
						}catch(Exception e){
							browserType = "LOCAL";
						}
					}
					
					/* Append browser type. */
					retBuf.append("<td>");
					retBuf.append(browserType);
					retBuf.append("</td>");
					
					/* Start Date*/
					Date startDate = testObj.getStartDate();
					retBuf.append("<td>");
					retBuf.append(this.getDateInStringFormat(startDate));
					retBuf.append("</td>");
					
					/* End Date*/
					Date endDate = testObj.getEndDate();
					retBuf.append("<td>");
					retBuf.append(this.getDateInStringFormat(endDate));
					retBuf.append("</td>");
					
					/* Execute Time */
					long deltaTime = endDate.getTime() - startDate.getTime();
					String deltaTimeStr = this.convertDeltaTimeToString(deltaTime);
					retBuf.append("<td>");
					retBuf.append(deltaTimeStr);
					retBuf.append("</td>");
					
					retBuf.append("</tr>");
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}finally
		{
			return retBuf.toString();
		}
	}

	/* Get date string format value. */
	private String getDateInStringFormat(Date date)
	{
		StringBuffer retBuf = new StringBuffer();
		if(date==null)
		{
			date = new Date();
		}
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		retBuf.append(df.format(date));
		return retBuf.toString();
	}
	
	/* Convert long type deltaTime to string with format hh:mm:ss. */
	private String convertDeltaTimeToString(long deltaTime)
	{
		StringBuffer retBuf = new StringBuffer();
		
		long milli = deltaTime;
		
		long seconds = deltaTime / 1000;
		
		long minutes = seconds / 60;
		
		long hours = minutes / 60;
		
		retBuf.append(hours + ":" + minutes + ":" + seconds + ":" + milli);
		
		return retBuf.toString();
	}
	
	/* Get test method summary info. */
	private String getTestMehodSummary(List<ISuite> suites)
	{
		StringBuffer retBuf = new StringBuffer();
		
		try
		{
			for(ISuite tempSuite: suites)
			{
				retBuf.append("<tr><td colspan=7><center><b>" + tempSuite.getName() + "</b></center></td></tr>");
				
				Map<String, ISuiteResult> testResults = tempSuite.getResults();
				
				for (ISuiteResult result : testResults.values()) {
					
					ITestContext testObj = result.getTestContext();

					String testName = testObj.getName();
					
					/* Get failed test method related data. */
					IResultMap testFailedResult = testObj.getFailedTests();
					String failedTestMethodInfo = this.getTestMethodReport(testName, testFailedResult, false, false);
					retBuf.append(failedTestMethodInfo);
					
					/* Get skipped test method related data. */
					IResultMap testSkippedResult = testObj.getSkippedTests();
					String skippedTestMethodInfo = this.getTestMethodReport(testName, testSkippedResult, false, true);
					retBuf.append(skippedTestMethodInfo);
					
					/* Get passed test method related data. */
					IResultMap testPassedResult = testObj.getPassedTests();
					String passedTestMethodInfo = this.getTestMethodReport(testName, testPassedResult, true, false);
					retBuf.append(passedTestMethodInfo);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}finally
		{
			return retBuf.toString();
		}
	}
	
	/* Get failed, passed or skipped test methods report. */
	private String getTestMethodReport(String testName, IResultMap testResultMap, boolean passedReault, boolean skippedResult)
	{
		StringBuffer retStrBuf = new StringBuffer();
		
		String resultTitle = testName;
		
		String color = "green";
		
		if(skippedResult)
		{
			resultTitle += " - Skipped ";
			color = "yellow";
		}else
		{
			if(!passedReault)
			{
				resultTitle += " - Failed ";
				color = "red";
			}else
			{
				resultTitle += " - Passed ";
				color = "green";
			}
		}
		
		retStrBuf.append("<tr bgcolor=" + color + "><td colspan=7><center><b>" + resultTitle + "</b></center></td></tr>");
			
		Set<ITestResult> testResultSet = testResultMap.getAllResults();
			
		for(ITestResult testResult : testResultSet)
		{
			String testClassName = "";
			String testMethodName = "";
			String startDateStr = "";
			String executeTimeStr = "";
			String paramStr = "";
			String reporterMessage = "";
			String exceptionMessage = "";
			
			//Get testClassName
			testClassName = testResult.getTestClass().getName();
				
			//Get testMethodName
			testMethodName = testResult.getMethod().getMethodName();
				
			//Get startDateStr
			long startTimeMillis = testResult.getStartMillis();
			startDateStr = this.getDateInStringFormat(new Date(startTimeMillis));
				
			//Get Execute time.
			long deltaMillis = testResult.getEndMillis() - testResult.getStartMillis();
			executeTimeStr = this.convertDeltaTimeToString(deltaMillis);
				
			//Get parameter list.
			Object paramObjArr[] = testResult.getParameters();
			for(Object paramObj : paramObjArr)
			{
				paramStr += (String)paramObj;
				paramStr += " ";
			}
				
			//Get reporter message list.
			List<String> repoterMessageList = Reporter.getOutput(testResult);
			for(String tmpMsg : repoterMessageList)				
			{
				reporterMessage += tmpMsg;
				reporterMessage += " ";
			}
				
			//Get exception message.
			Throwable exception = testResult.getThrowable();
			if(exception!=null)
			{
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				exception.printStackTrace(pw);
				
				exceptionMessage = sw.toString();
			}
			
			retStrBuf.append("<tr bgcolor=" + color + ">");
			
			/* Add test class name. */
			retStrBuf.append("<td>");
			retStrBuf.append(testClassName);
			retStrBuf.append("</td>");
			
			/* Add test method name. */
			retStrBuf.append("<td>");
			retStrBuf.append(testMethodName);
			retStrBuf.append("</td>");
			
			/* Add start time. */
			retStrBuf.append("<td>");
			retStrBuf.append(startDateStr);
			retStrBuf.append("</td>");
			
			/* Add execution time. */
			retStrBuf.append("<td>");
			retStrBuf.append(executeTimeStr);
			retStrBuf.append("</td>");
			
			/* Add parameter. */
			retStrBuf.append("<td>");
			retStrBuf.append(paramStr);
			retStrBuf.append("</td>");
			
			/* Add reporter message. */
			retStrBuf.append("<td>");
			retStrBuf.append(reporterMessage);
			retStrBuf.append("</td>");
			
			/* Add exception message. */
			retStrBuf.append("<td>");
			retStrBuf.append(exceptionMessage);
			retStrBuf.append("</td>");
			
			retStrBuf.append("</tr>");

		}
		
		return retStrBuf.toString();
	}
	
	/* Convert a string array elements to a string. */
	private String stringArrayToString(String strArr[])
	{
		StringBuffer retStrBuf = new StringBuffer();
		if(strArr!=null)
		{
			for(String str : strArr)
			{
				retStrBuf.append(str);
				retStrBuf.append(" ");
			}
		}
		return retStrBuf.toString();
	}

}



//another class

package reporting;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javaCode.CommonFunctions;

import org.apache.poi.common.usermodel.Hyperlink;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import testScripts.driverScript;
import testScripts.generateTestNG;

	/*GUIDE TO USE THE BELOW EXCEL REPORTING FUNCTIONS
	______________________________________________________________________________________________________________________  
	FAILED REPORTS:
	
	//////////////////////////Reporting//////////////////////////
	String str_fileName = "testFunction";
	String str_screenShotFileName = CommonFunctions.takeScreenShotMethod(driver, str_fileName);
	Report.failDetailedReporting(driver, str_screenShotFileName, "Add as many columns as needed!");
	Report.Log_ReportNG("FAIL", "Search box is not open!", driver, str_screenShotFileName);
	System.err.println("Search box is not open!");
	/////////////////////////////////////////////////////////////
	______________________________________________________________________________________________________________________
	PASSED REPORTS:
	
	/////////////////////Reporting/////////////////////////
	Report.Log_ReportNG("PASS", "", str_pageName, "Test title", "Test description", "Pass", "Script", str_captureFailureMessage);
	Report.passDetailedReporting(str_pageName, "Test title", "Test description", "PASS", "", str_captureFailureMessage);
	System.out.println(str_pageName+ " : Test title : Test description : PASS ");
	//////////////////////////////////////////////////////		
	*/

public class Report
{

	public static void passDetailedReporting(String... args) throws IOException, InvalidFormatException {
		
		String file = driverScript.reportFileName;
		
		FileInputStream reportInputStream = new FileInputStream(file);
		Workbook reportWorkBook = WorkbookFactory.create(reportInputStream);

		Sheet reportSpreadSheet = reportWorkBook.getSheetAt(0);
		
		int int_rowCount = reportSpreadSheet.getLastRowNum();
		Row row = reportSpreadSheet.createRow(++int_rowCount);
		
		//Style
		CellStyle style = reportWorkBook.createCellStyle();
		Font font = reportWorkBook.createFont();
		style.setFont(font);
		style.setFillForegroundColor(IndexedColors.RED.index);
		font.setColor(HSSFColor.RED.index);
		
		List<String> str_dataList = new ArrayList<String>();

		for (String i : args)
			str_dataList.add(i);

		for(int i=0;i<str_dataList.size();i++){
			
			Cell cell = row.createCell(i);
			cell.setCellValue(str_dataList.get(i));
					
		}

		reportInputStream.close();
		FileOutputStream outputStream = new FileOutputStream(file);
		reportWorkBook.write(outputStream);
		outputStream.close();
	}
	
	public static void failDetailedReporting(WebDriver driver, String str_screenShotFileName, String... args) throws IOException, InvalidFormatException {

		String file = driverScript.reportFileName;
				
		FileInputStream reportInputStream = new FileInputStream(file);
		Workbook reportWorkBook = WorkbookFactory.create(reportInputStream);

		Sheet reportSpreadSheet = reportWorkBook.getSheetAt(0);
		
		int int_rowCount = reportSpreadSheet.getLastRowNum();
		Row row = reportSpreadSheet.createRow(++int_rowCount);
		
		//Style
		CellStyle style = reportWorkBook.createCellStyle();
		Font font = reportWorkBook.createFont();
		style.setFont(font);
		style.setFillForegroundColor(IndexedColors.RED.index);
		font.setColor(HSSFColor.RED.index);
		
		List<String> str_dataList = new ArrayList<String>();

		for (String i : args)
			str_dataList.add(i);

		for(int i=0;i<str_dataList.size();i++){
	
			//row = spreadSheet.createRow(++int_rowCount);
				
			//Cell 1
			Cell cell = row.createCell(i);
	
			if(i==2){
			//Cell 5
				cell.setCellValue(str_dataList.get(i));
				row.getCell(2).setCellStyle(style);
			}else if(i==2 || i==3  || i==4 ){
				//Cell 5
				cell.setCellValue(str_dataList.get(i));
				CreationHelper createHelper21 = reportWorkBook.getCreationHelper();
				Hyperlink link21 = createHelper21.createHyperlink(Hyperlink.LINK_FILE);
				String screenShotURL = "C:/Working/" + "ScreenShots/"+CommonFunctions.getTimeStamp("yyyyMMdd") +"/"+str_screenShotFileName;
				String screenShotCorrected = CommonFunctions.convertToFileURL(screenShotURL);
				link21.setAddress(screenShotCorrected);
				cell.setHyperlink((org.apache.poi.ss.usermodel.Hyperlink) link21);
				row.getCell(i).setCellStyle(style);
				
			}else{
				cell.setCellValue(str_dataList.get(i));
				
			}
		}
		
		reportInputStream.close();
		FileOutputStream outputStream = new FileOutputStream(file);
		reportWorkBook.write(outputStream);
		outputStream.close();
	}
	
	public static void Log_ReportNG(String str_status,
			String str_ScnSht_Folder, String... args) throws IOException {

		if(str_status.equals("PASS")){
			Reporter.log("<br> <font color=green> "+ args[0] +" : "+ args[1] +" : "+ args[2] +" </font> </br>");
		}else if(str_status.equals("FAIL")){
	
			String screenShotURL = generateTestNG.str_folderPath + "ScreenShots/"+CommonFunctions.getTimeStamp("yyyyMMdd") +"/"+str_ScnSht_Folder;
			Reporter.log("<br> <font color=red> <a href=\""+screenShotURL+"\""+ args[0] +" : "+ args[1] +" : "+ args[2]+" : "+ args[3] +"</a> </font>");
			//Reporter.log("<font color=red> "+ args[0] +" : "+ args[1] +" : "+ args[2] + " </font> <br>");
		}
	}
}



//another files



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=gbk" />
    <title>TestNG Report</title>
    <style type="text/css">table {margin-bottom:10px;border-collapse:collapse;empty-cells:show}th,td {border:1px solid #009;padding:.25em .5em}th {vertical-align:bottom}td {vertical-align:top}table a {font-weight:bold}.stripe td {background-color: #E6EBF9}.num {text-align:right}.passedodd td {background-color: #3F3}.passedeven td {background-color: #0A0}.skippedodd td {background-color: #DDD}.skippedeven td {background-color: #CCC}.failedodd td,.attn {background-color: #F33}.failedeven td,.stripe .attn {background-color: #D00}.stacktrace {white-space:pre;font-family:monospace}.totop {font-size:85%;text-align:center;border-bottom:2px solid #000}</style>
  </head>
  <body>
    <table>
      <thead>
        <tr>
        
          <th>Test Name</th>
           <th>Steps Passed</th>
          <th>Steps Failed</th>
          <th>Browser</th>
          <th>Start Time</th>
          <th>End Time</th>
          <th>Execute Time (mm:hh:ss:ms)</th>
        </tr>
       </thead> 
       $Test_Case_Summary$
    </table>
    
  </body>
</html>



//files

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=gbk" />
    <title>TestNG Report</title>
    <style type="text/css">table {margin-bottom:10px;border-collapse:collapse;empty-cells:show}th,td {border:1px solid #009;padding:.25em .5em}th {vertical-align:bottom}td {vertical-align:top}table a {font-weight:bold}.stripe td {background-color: #E6EBF9}.num {text-align:right}.passedodd td {background-color: #3F3}.passedeven td {background-color: #0A0}.skippedodd td {background-color: #DDD}.skippedeven td {background-color: #CCC}.failedodd td,.attn {background-color: #F33}.failedeven td,.stripe .attn {background-color: #D00}.stacktrace {white-space:pre;font-family:monospace}.totop {font-size:85%;text-align:center;border-bottom:2px solid #000}</style>
  </head>
  <body>
    <table>
      <tbody>
        <tr>
          <th>Test</th>
          <th># Passed</th>
          <th># Failed</th>
          <th>Time (ms)</th>
          <th>Included Groups</th>
          <th>Excluded Groups</th>
        </tr>
        <tr>
          <th colspan="7">MainSuite</th>
        </tr>
        <tr>
          <td><a href="#t0">MainSuiteTest</a></td>
          <td class="num">6</td>
          <td class="num">0</td>
          <td class="num attn">2</td>
          <td class="num">92</td>
          <td><br />
          </td>
          <td><br />
          </td>
        </tr>
      </tbody>
    </table>
    
  </body>
</html>



//another class

package testScripts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import javaCode.CommonFunctions;
import javaCode.LinksDeepLinking;
import reporting.Report;

public class driverScript
{

	public static String fileName = "AllPages_testData.xlsx";
	public static String screenShotFolder = "";
	public static String reportsFolder = "";
	
	private static int sauceFlag = 0;
	private int int_Iter_Global = 0;

	public static Map<String, Integer> failedStepsMap = generateTestNG.failedStepsMap;
	public static Map<String, Integer> passedStepsMap = generateTestNG.passedStepsMap;
	
	WebDriver driver;

	public static String str_SauceAccount = "Shan";

	public static String reportFileName = "";
	
	@BeforeSuite
	public void beforeSuite() throws IOException, InterruptedException{

		try{
			Thread.sleep(1000);
			screenShotFolder = CommonFunctions.createAFolderInProjectDir("C:/Working/","ScreenShots") + "/"+CommonFunctions.getTimeStamp("yyyyMMdd")+"/";
			Thread.sleep(1000);
			reportsFolder = CommonFunctions.createAFolderInProjectDir("C:/Working/","Reports") + "/"+CommonFunctions.getTimeStamp("yyyyMMdd")+"/";
			CommonFunctions.createAFolderInProjectDir("C:/Working/"+"ScreenShots/",CommonFunctions.getTimeStamp("yyyyMMdd"));
			CommonFunctions.createAFolderInProjectDir("C:/Working/"+"Reports/",CommonFunctions.getTimeStamp("yyyyMMdd"));
			Thread.sleep(1000);
		}catch(Exception e){
			System.err.println("Severe error occured while initializing reports, please call POC. Testing cannot proceed!");
			Assert.assertFalse(true);
		}
	}	
	
    @Test
    public void myTest() throws IOException, InvalidFormatException{
    	
    	CommonFunctions cf = new CommonFunctions();

    	int int_executeHighLevelFlag = 0;

		System.setProperty("org.uncommons.reportng.escape-output", "false");
    	
		long int_Current_ThreadID = Thread.currentThread().getId();
		long int_ThreadIndex = 0;
		System.out.println("Thread Index before: " + int_Current_ThreadID );
		//result = testStatement ? value1 : value2;
		int_ThreadIndex = int_Current_ThreadID >= 11 ? int_Current_ThreadID - 11 : int_Current_ThreadID - 1;
		
		System.out.println("Thread Index after: " + int_ThreadIndex);    	
    	
		int_Iter_Global = (int) int_ThreadIndex;

		ArrayList<String> arrayList_BrowserTypes = new ArrayList<String>();
    	ArrayList<String> arrayList_BrowserTypes_LOCAL = new ArrayList<String>();
    	
		try {
			String str_TestDataFileName = fileName;
			String str_TestDataSheetName =  "TestSet";
			
			String testData_filePath = generateTestNG.dataFilePath;		
			String fileName = str_TestDataFileName; //"HybridFrameWork_TestData.xlsx";
			String sheetName_TS = str_TestDataSheetName; //"TestSet";
			
			File file_TS =    new File(generateTestNG.dataFilePath+fileName);
			//Testing...
			FileInputStream inputStream = new FileInputStream(file_TS);
			//result = testStatement ? value1 : value2;
			Workbook myWorkBook = fileName.substring(fileName.indexOf(".")).equals(".xlsx") ? new XSSFWorkbook(inputStream) 
								: fileName.substring(fileName.indexOf(".")).equals(".xlsx") ? new HSSFWorkbook(inputStream)
								: null;
							
			Sheet mySheet_TS = myWorkBook.getSheet(sheetName_TS);

			int rowCount_TS = mySheet_TS.getLastRowNum()- mySheet_TS.getFirstRowNum();
			
			System.err.println("Row Count from TestSet: "+ rowCount_TS);
						
			int int_CellIndex_UserType = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "User Type");
			int int_CellIndex_Execute = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Execute");
			int int_CellIndex_Execute_HL = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Execute_HL");
			int int_CellIndex_TestName = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Test Name");
			int int_CellIndex_UserId = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "UserID");
			int int_CellIndex_Pwd = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Pwd");
			int int_CellIndex_Environment = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Environment");
			int int_CellIndex_IE = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "IE");
			int int_CellIndex_Chrome = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Chrome");
			int int_CellIndex_SauceIE = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "SAUCE_IE");
			int int_CellIndex_SauceChrome = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "SAUCE_CHROME");
			String str_Environment = null;
			
			for (int iRow_TS=1;iRow_TS<=rowCount_TS;iRow_TS++)
			{
				System.out.println(iRow_TS);

				String str_Execute_TS = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Execute).toString(); 

				String str_Execute_HL_TS = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Execute_HL).toString();

				if(str_Execute_TS.trim().equalsIgnoreCase("Y")){
					//result = testStatement ? value1 : value2;
					int_executeHighLevelFlag = str_Execute_HL_TS.trim().equalsIgnoreCase("Y") ? 1
											 : str_Execute_HL_TS.trim().equalsIgnoreCase("N") ? 0
											 : null;
					
					String str_UserType	= mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_UserType).toString();
					String str_TestName = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_TestName).toString();
					String str_UserID = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_UserId).toString();
					String str_Pwd = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Pwd).toString();
					str_Environment = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Environment).toString();
					
					String str_IE = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_IE).toString();    		
					String str_Chrome = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Chrome).toString();
					
					String str_SAUCE_IE = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_SauceIE).toString();
					String str_SAUCE_CHROME = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_SauceChrome).toString();
					
					if (str_IE.equalsIgnoreCase("Y"))
					{
						if(!arrayList_BrowserTypes.contains("LOCAL")) 
						{
							arrayList_BrowserTypes.add("LOCAL");	
						}
						arrayList_BrowserTypes_LOCAL.add("IE"+"#"+str_UserID +"#" + str_Pwd + "#" + str_TestName + "#" + str_UserType);
					}
					if (str_Chrome.equalsIgnoreCase("Y"))
					{
						if(!arrayList_BrowserTypes.contains("LOCAL")) 
						{
							arrayList_BrowserTypes.add("LOCAL");	
						}
						arrayList_BrowserTypes_LOCAL.add("CHROME"+"#"+str_UserID +"#" + str_Pwd+ "#" + str_TestName+ "#" + str_UserType);
					}
					
					if (!str_SAUCE_IE.equalsIgnoreCase("N"))
					{
						arrayList_BrowserTypes.add("SAUCE_IE" + "#" +str_UserID +"#" + str_Pwd + "#" + str_TestName+ "#" + str_UserType + "#" + str_SAUCE_IE);
					}
						
					if (!str_SAUCE_CHROME.equalsIgnoreCase("N"))
					{
						arrayList_BrowserTypes.add("SAUCE_CHROME"  + "#" +str_UserID +"#" + str_Pwd + "#" + str_TestName+ "#" + str_UserType+ "#" + str_SAUCE_CHROME);
					}
			
				} //if (str_Execute_TS.trim().equalsIgnoreCase("Y"))
				
			} //for (int iRow_TS=1;iRow_TS<=rowCount_TS;iRow_TS++)			
			
			System.out.println(int_Iter_Global  + " > Data from Array: " + arrayList_BrowserTypes.get(int_Iter_Global));
			
			String str_UserData = arrayList_BrowserTypes.get(int_Iter_Global);
			passedStepsMap.put(str_UserData, 0);
			failedStepsMap.put(str_UserData, 0);

			String[] arr_UserData = str_UserData.split("#"); //"SAUCE_IE" + "#" +str_UserID +"#" + str_Pwd + "#" + str_TestName+ "#" + str_UserType + "#" + str_SAUCE_IE
			
			String str_BrowserType = arr_UserData[0]; //SAUCE_IE
			
			String str_URL 
					= str_Environment.equalsIgnoreCase("PP") ? "https://markets-pp.ml.com/" 
					: str_Environment.equalsIgnoreCase("PROD") ? "https://markets.ml.com/"
					: "";
			reportFileName = cf.CreateDetailedFile(str_Environment, reportsFolder, "AllPagesScreenShot_"+CommonFunctions.getTimeStamp("yyyyMMdd_HHmm")+"_"+CommonFunctions.randomString(6)+".xlsx");

			String str_UserName = null;
			String str_Pwd = null;			
			String str_TestName = null;
					
			if(str_BrowserType.startsWith("SAUCE_")) 
			{
				sauceFlag = 1;

				str_UserName = arr_UserData[1];
				str_Pwd = arr_UserData[2];
				str_TestName = arr_UserData[3];  
				String str_TestName_N_BrowserType_N_UserName = str_TestName + "#" + str_BrowserType + "#" + str_UserName;
				
				String str_PlatformNBrowserVersion = arr_UserData[5]; //Windows 7;11.0
				String str_Platform = str_PlatformNBrowserVersion.split(";")[0];
				String str_Version = str_PlatformNBrowserVersion.split(";")[1];
				String str_deviceName = null;
				String str_deviceOrientation = null;
				
				String str_UserType = arr_UserData[4];
				
				String str_BrowserType2 = str_BrowserType.split("_")[1];
				driver = CommonFunctions.openBrowser_SAUCE(str_SauceAccount,
						str_BrowserType2, str_Version, str_Platform, str_URL,
						str_deviceName, str_deviceOrientation,
						str_TestName_N_BrowserType_N_UserName, str_UserName);
				
				CommonFunctions.Login(driver, str_UserName, str_Pwd, str_URL, str_UserType);
				String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:ms").format(new Date());
				Reporter.log("<br> <font color=green>"+ "TimeStamp: [" + timeStamp + "]" + "</font> </br>");	
				
				CommonFunctions.waitUntil_jQueryActiveNReadyStateComplete(driver, 120000);
				
				System.out.println(str_TestName + "; Title: " + driver.getTitle());
				//result = testStatement ? value1 : value2;
				int_executeHighLevelFlag = !(arrayList_BrowserTypes.get(int_Iter_Global).contains("High Level")) ? 0 : int_executeHighLevelFlag;
				
				execute_DataSheet(driver, str_UserData, str_BrowserType, str_UserType, myWorkBook, str_TestName, str_UserType,
						str_TestDataFileName, testData_filePath, str_URL, sauceFlag, int_executeHighLevelFlag,
						str_UserName, str_Pwd, reportFileName);

				CommonFunctions.Logout(driver, str_UserName);
				
				if (driver != null) 
				{
					driver.close();
					driver.quit();
					driver = null;				
				}
				
			} //if(str_BrowserType.startsWith("SAUCE_"))

			else if (str_BrowserType.startsWith("LOCAL"))
			{
				
				System.out.println("FROM LOCAL IF CONDITION,,,,,,,");
				
				System.out.println("LOCAL DATA INSIDE IF CONDITION: " + arrayList_BrowserTypes_LOCAL);
			
				for (String str_UserAndBrowserData : arrayList_BrowserTypes_LOCAL) 
				{
					
					System.out.println("INSIDE FOR LOOP....data: " + str_UserAndBrowserData);
					str_BrowserType = str_UserAndBrowserData.split("#")[0];
					str_UserName = str_UserAndBrowserData.split("#")[1];
					str_Pwd = str_UserAndBrowserData.split("#")[2];
					str_TestName = str_UserAndBrowserData.split("#")[3];
					
					String str_UserType = str_UserAndBrowserData.split("#")[4];
					
					System.out.println("str_BrowserType: " + str_BrowserType + "; str_UserName: " + str_UserName + "; str_TestName: "+ str_TestName);
					
					driver = CommonFunctions.openBrowser(str_BrowserType, str_URL);

					CommonFunctions.Login(driver, str_UserName,
							str_Pwd, str_URL, str_UserType);
					
					String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:ms").format(new Date());
					Reporter.log("<br> <font color=green>"+ "TimeStamp: [" + timeStamp + "]" + "</font> </br>");	
					CommonFunctions.waitUntil_jQueryActiveNReadyStateComplete(driver, 120000);
					
					System.out.println(str_TestName + "; Title: " + driver.getTitle());
					
					//String str_BrowserType_N_UserName = str_BrowserType+ "_" + str_UserName;
					//String str_TimeStamp = CommonFunctions.getTimeStamp("yyyy-MM-dd-HH-mm-ss");
					//String reportLocation = str_ReportFolderPath + "\\"+ str_BrowserType_N_UserName + "_" + str_UserType +"_Report_["+ str_TimeStamp + "].html";
					execute_DataSheet(driver, str_UserData, str_BrowserType, str_UserType, myWorkBook, str_TestName,
							str_UserType, str_TestDataFileName,
							testData_filePath, str_URL,
							sauceFlag, int_executeHighLevelFlag, str_UserName, str_Pwd, reportFileName);

					Thread.sleep(2000);
					CommonFunctions.Logout(driver, str_UserName);
					
					if (driver != null) 
					{
						driver.close();
						driver.quit();
						driver = null;				
					}
				}
			}

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute_DataSheet(WebDriver driver, String str_UserData, String str_browser, String str_UserType, Workbook myWorkBook,
			String str_TestName, String str_Enviroment, String str_TestDataFileName, String testData_filePath,
			String str_URL, int sauceFlag, int executeHighLevelFlag, String str_UserName, String str_Pwd,
			String reportFileName) throws IOException, InvalidFormatException, InterruptedException {

		System.out.println("**********" + executeHighLevelFlag + "//" + "**************");
	    
		String str_sheetName_TD = "TestData";
		String str_TestCaseName = str_TestName; //"Historical Close Comparison";

	    Sheet mySheet_TD = myWorkBook.getSheet(str_sheetName_TD);
		int int_rowCountDataSheet = mySheet_TD.getLastRowNum()-mySheet_TD.getFirstRowNum();
	    
	    System.out.println("Row Count in Test Data: "+ int_rowCountDataSheet);

	    int int_StartRow = 0;
	    int int_EndRow = 0;
	    
	    if(!(executeHighLevelFlag==1)){
		    for (int i=0; i < int_rowCountDataSheet+1; i++) {
		    	if (mySheet_TD.getRow(i).getCell(1).toString().equalsIgnoreCase(str_TestCaseName)){
		    		int_StartRow = i;
		    		break;
		    	}
		    }
		    for (int j=int_rowCountDataSheet; j>=0; j--) {
		    	if (mySheet_TD.getRow(j).getCell(1).toString().equalsIgnoreCase(str_TestCaseName)){
		    		int_EndRow = j;
		    		break;
		    	}
		    }
	    }else{
		
	    	int_StartRow = 2;
	    	int_EndRow = int_rowCountDataSheet;
	    }
	    System.out.println("Start Row: "+ int_StartRow + " End Row: "+ int_EndRow);
	    int int_CellIndex_UserType_TD = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, "TestData", testData_filePath, 0, "Exe_" + str_Enviroment);
	    
	    String str_ExecutionFlag = null;							    
	    
	    if(executeHighLevelFlag==1){

		}else if(executeHighLevelFlag==0){
			for (int iRow= int_StartRow+1; iRow<=int_EndRow; iRow=iRow+1){
		    	 
		    	str_ExecutionFlag = mySheet_TD.getRow(iRow).getCell(int_CellIndex_UserType_TD).toString().trim();
		    	
		    	if (str_ExecutionFlag.contains("Y") && str_ExecutionFlag!="")
		    	{

		    		int int_KeywordColNum = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, "TestData", testData_filePath, 0, "Business Component");
	
		    		String str_Keyword = mySheet_TD.getRow(iRow).getCell(int_KeywordColNum).toString();
		    		
		    		int int_TotalCols = mySheet_TD.getRow(iRow).getLastCellNum();
	
		    		ArrayList<String> arrayList_ParametersData = new ArrayList<String>();
		    		
			    	for (int iCol = int_KeywordColNum+1 ;iCol<int_TotalCols; iCol++ ){
			    		
			    		String str_CellValue = mySheet_TD.getRow(iRow).getCell(iCol).toString();

			    		if (!str_CellValue.isEmpty())	
			    		{
			    			arrayList_ParametersData.add(str_CellValue);
			    		}
			    	}
			    	
			    	String [] arrDataFromSheet = arrayList_ParametersData.size() > 0 ? CommonFunctions.getArrayListItemsAsArray(arrayList_ParametersData) : null;
		    	 
					try {

						if(str_Keyword.equals("LinksDeepLinking")){
							LinksDeepLinking.takePageScreenShot(driver, str_UserData, str_URL, arrDataFromSheet[0], arrDataFromSheet[1]);
						}
						
					} catch (Exception e) {
						//////////////////////////Reporting//////////////////////////
						String str_fileName = "driverScript";
						String str_screenShotFileName = CommonFunctions.takeScreenShotMethod(driver, str_fileName);
						System.err.println("Exception occurred: "+e.getMessage());
						//Report.Log_ReportNG("FAIL", str_screenShotFileName, "Exception occurred: "+e.getMessage());
						/////////////////////////////////////////////////////////////
						e.printStackTrace();
					}
				} // if (str_BusComp.equalsIgnoreCase("Y") && str_BusComp!="")
			} // for (int iRow= int_StartRow+1; iRow<int_EndRow; iRow=iRow+2)
		}
    }

	@AfterSuite
	public void createSummary() throws IOException{

		//Runtime.getRuntime().exec("cmd /c start \"\" C:/Users/zk2umiw/workspace3/glis2_automation/ZipFileProcess.bat");

	}
	
}


//another class


package testScripts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javaCode.CommonFunctions;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.TestNG;
import org.testng.annotations.Test;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

public class generateTestNG {

	public static String str_folderPath = System.getProperty("user.dir");

	public static Map<String, Integer> failedStepsMap = new ConcurrentHashMap<String, Integer>();
	public static Map<String, Integer> passedStepsMap = new ConcurrentHashMap<String, Integer>();

	public static String dataFilePath = "";

	@Test
	public static void main() throws IOException 
	{
		
		if(str_folderPath.contains("target")){
			str_folderPath = str_folderPath.replace("target", "");
		}

		dataFilePath = str_folderPath + "src/test/java/testData/";
		
		int mb = 1024*1024;
		
		//Getting the runtime reference from system
		Runtime runtime = Runtime.getRuntime();
		
//		System.out.println("##### Heap utilization statistics [MB] #####");
//		
//		//Print used memory
//		System.out.println("Used Memory:" 
//			+ (runtime.totalMemory() - runtime.freeMemory()) / mb);
//
//		//Print free memory
//		System.out.println("Free Memory:" 
//			+ runtime.freeMemory() / mb);
//		
//		//Print total available memory
//		System.out.println("Total Memory:" + runtime.totalMemory() / mb);
//
//		//Print Maximum available memory
//		System.out.println("Max Memory:" + runtime.maxMemory() / mb);
	
		generateTestNG testNG1 = new generateTestNG();
		
		Map<String, String> testngParams = new HashMap<String, String>();
		
		testNG1.genTestNG(testngParams);
	
	}
	
	
	public static void genTestNG(Map<String,String> testngParams) throws IOException
	{
		
		ArrayList<String> arrayList_BrowserTypes = new ArrayList<String>();
		boolean bln_DataIssue = false;
		
		try {
			
			//Create an instance on TestNG
			 TestNG myTestNG = new TestNG();
			 
			//Create an instance of XML Suite and assign a name for it.
			 XmlSuite mySuite = new XmlSuite();
			 mySuite.setName("MySuite");
			 mySuite.addListener("org.uncommons.reportng.HTMLReporter");
			 //mySuite.addListener("org.uncommons.reportng.JUnitXMLReporter");		 
			 mySuite.addListener("reporting.CustomTestNGReporter");
			 mySuite.setParallel("tests");
			 mySuite.setPreserveOrder("true");
			 mySuite.setThreadCount(50);
			 
			String str_TestDataFileName = driverScript.fileName;
			String str_TestDataSheetName =  "TestSet";
			
			// Read data from TestSet and Pass -->  Test Name if Execute is Y; Env; Browser
			
			String testData_filePath = dataFilePath;		
			String fileName = str_TestDataFileName; //"HybridFrameWork_TestData.xlsx";
			String sheetName_TS = str_TestDataSheetName; //"TestSet";
			System.err.println(">>>>>>>>>>>>>>>>>>>>>>"+testData_filePath+fileName);
			//Create an object of File class to open xlsx file
			String str_fileTest = testData_filePath+fileName;
			File file_TS =    new File(str_fileTest);
			
			//Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file_TS);
			Workbook myWorkBook = null;
			
			//Find the file extension by splitting file name in substring and getting only extension name
			String fileExtensionName = fileName.substring(fileName.indexOf("."));

			//Check condition if the file is xlsx file
			if(fileExtensionName.equals(".xlsx")){
			    //If it is xlsx file then create object of XSSFWorkbook class
				myWorkBook = new XSSFWorkbook(inputStream);
			}
			//Check condition if the file is xls file
			else if(fileExtensionName.equals(".xls")){
			    //If it is xls file then create object of XSSFWorkbook class
				myWorkBook = new HSSFWorkbook(inputStream);
			}				

			//Read sheet inside the workbook by its name
			Sheet mySheet_TS = myWorkBook.getSheet(sheetName_TS);

			//Find number of rows in excel file
			int rowCount_TS = mySheet_TS.getLastRowNum()- mySheet_TS.getFirstRowNum();
			
			System.out.println("Row Count from TestSet: "+ rowCount_TS);
			System.err.println(mySheet_TS.getPhysicalNumberOfRows());
			//String cellData = mySheet.getRow(1).getCell(1).toString();
			//Print("Cell Data: "+ cellData);
			//Print(mySheet.getRow(24).getCell(1).toString());
			
			//Get the range of a given Test Name(First and Last Row)

			//String str_TestCaseName = "Swaption Closing Skew";	    

			//int int_CellIndex_UserType = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "User Type");
			int int_CellIndex_UserType = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "User Type");
			int int_CellIndex_Execute = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Execute");
			int int_CellIndex_TestName = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Test Name");
			int int_CellIndex_UserId = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "UserID");
			int int_CellIndex_Pwd = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Pwd");
			int int_CellIndex_IE = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "IE");
			int int_CellIndex_Chrome = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Chrome");
			int int_CellIndex_SauceIE = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "SAUCE_IE");
			int int_CellIndex_SauceChrome = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "SAUCE_CHROME");				
			
		for (int iRow_TS=1;iRow_TS<=rowCount_TS;iRow_TS++)
			{

				//Read Test Name and check if Execute is Y or N
				String str_Execute_TS = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Execute).toString();
				
				if (str_Execute_TS.trim().equalsIgnoreCase("Y"))
				{
					//Get Environment, IE, Chrome, FF and Sauce Browsers

					String str_TestName = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_TestName).toString();
					String str_UserID = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_UserId).toString();
					String str_UserType = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_UserType).toString();
					String str_Pwd = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Pwd).toString();
					//String str_Enviroment = mySheet_TS.getRow(iRow_TS).getCell(5).toString(); 
					String str_IE = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_IE).toString();    		
					String str_Chrome = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Chrome).toString();
					
					String str_SAUCE_IE = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_SauceIE).toString();
					String str_SAUCE_CHROME = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_SauceChrome).toString();
				
					//Build an Array with Browser Types that needs to be executed
					
					//if (str_IE.equalsIgnoreCase("Y") && str_SAUCE_IE.equalsIgnoreCase("N") && str_SAUCE_CHROME.equalsIgnoreCase("N")) 
					//str_Chrome.equalsIgnoreCase("Y") && str_SAUCE_IE.equalsIgnoreCase("N") && str_SAUCE_CHROME.equalsIgnoreCase("N"))
					
					if (str_IE.equalsIgnoreCase("Y")) 
					{
						if(!arrayList_BrowserTypes.contains("LOCAL"))
						{
							arrayList_BrowserTypes.add("LOCAL");
						}
					}
					
					
					if (str_Chrome.equalsIgnoreCase("Y"))
					//if (str_Chrome.equalsIgnoreCase("Y") && str_SAUCE_IE.equalsIgnoreCase("N") && str_SAUCE_CHROME.equalsIgnoreCase("N"))
					{
						if(!arrayList_BrowserTypes.contains("LOCAL"))
						{
							arrayList_BrowserTypes.add("LOCAL");
						}
					}
					
					//if (str_IE.equalsIgnoreCase("N") && str_Chrome.equalsIgnoreCase("N") && !str_SAUCE_IE.equalsIgnoreCase("N"))
					if (!str_SAUCE_IE.equalsIgnoreCase("N"))							
					{
						arrayList_BrowserTypes.add("SAUCE_IE"  + "#" +str_UserID +"#" + str_Pwd + "#" + str_TestName+ "#" + str_UserType+ "#" + str_SAUCE_IE);
						//arrayList_BrowserTypes.add(str_TestName + "#" +str_UserID + "#SAUCE_IE" + "#" + str_SAUCE_IE);
					}
						
					//if (str_IE.equalsIgnoreCase("N") && str_Chrome.equalsIgnoreCase("N") && !str_SAUCE_CHROME.equalsIgnoreCase("N"))
					if (!str_SAUCE_CHROME.equalsIgnoreCase("N"))							
					{
						
						arrayList_BrowserTypes.add("SAUCE_CHROME"  + "#" +str_UserID +"#" + str_Pwd + "#" + str_TestName+ "#" + str_UserType+ "#" + str_SAUCE_CHROME);

						//arrayList_BrowserTypes.add(str_TestName + "#" +str_UserID + "#SAUCE_CHROME"  + "#" + str_SAUCE_CHROME);
					}
			
				} //if (str_Execute_TS.trim().equalsIgnoreCase("Y"))
				
			} //for (int iRow_TS=1;iRow_TS<=rowCount_TS;iRow_TS++)
			 
			if (!bln_DataIssue) 
			{
				int int_arraySize = arrayList_BrowserTypes.size();
				System.out.println("Total Combinations Selected...: " + int_arraySize);
				int int_Instances_To_Run = int_arraySize;
				int int_Iter = 0;
				
			 	//Create a list of XmlTests and add the Xmltest you created earlier to it.
			 	List<XmlTest> myTests = new ArrayList<XmlTest>();
			 	
				 for (int i = 0; i <int_Instances_To_Run; i++) 
				 {
					 //Create an instance of XmlTest and assign a name for it.
					 XmlTest myTest = new XmlTest(mySuite);
					 //myTest.setName("MyTest_fromLoop:"+i);
					 myTest.setName(arrayList_BrowserTypes.get(i));					 
					 failedStepsMap.put(arrayList_BrowserTypes.get(i), 0);
					 passedStepsMap.put(arrayList_BrowserTypes.get(i), 0);

					 //Add any parameters that you want to set to the Test.
					 //testngParams.put("str_Iter_Global", Integer.toString(i));
					 //myTest.setParameters(testngParams);
					 myTest.addParameter("str_Iter_Global"+i, Integer.toString(i));
					 
					//Create a list which can contain the classes that you want to run.
					List<XmlClass> myClasses = new ArrayList<XmlClass> ();
					myClasses.add(new XmlClass("testScripts.driverScript"));				 
					 
					//Assign that to the XmlTest Object created earlier.
					myTest.setXmlClasses(myClasses);
					 
					//Create a list of XmlTests and add the Xmltest you created earlier to it.
					 myTests.add(myTest);
				 }
					 
				 //System.out.println("testngParams: " + testngParams);
				 
				//add the list of tests to your Suite.
				 mySuite.setTests(myTests);
				 
				//Add the suite to the list of suites.
				 List<XmlSuite> mySuites = new ArrayList<XmlSuite>();
				 mySuites.add(mySuite);
				 
				//Set the list of Suites to the testNG object you created earlier.
				 myTestNG.setXmlSuites(mySuites);
				 
				//invoke run() - this will run your class.
				 myTestNG.run();
				
			}
			 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}













